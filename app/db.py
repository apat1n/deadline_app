from typing import List

from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class DeadlineEvent(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    deadline = db.Column(db.DateTime, nullable=False)


def add_event(event: DeadlineEvent):
    try:
        db.session.add(event)
        db.session.commit()
    finally:
        db.session.close()


def delete_event(id_: int):
    try:
        event = get_event_by_id(id_)
        db.session.delete(event)
        db.session.commit()
    finally:
        db.session.close()


def get_events() -> List[DeadlineEvent]:
    return DeadlineEvent.query.all()


def get_event_by_id(id_: int) -> DeadlineEvent:
    return DeadlineEvent.query.filter_by(id=id_).first()


def get_event_by_name(name: str) -> DeadlineEvent:
    return DeadlineEvent.query.filter_by(name=name).first()
