import os
from flask import send_file
from ics import Calendar, Event

from app.db import DeadlineEvent


def generate_ics_file(event: DeadlineEvent):
    c = Calendar()
    e = Event()
    e.name = event.name
    e.begin = event.deadline.strftime("%Y-%m-%d %H:%M:%S")
    c.events.add(e)

    if not os.path.exists("/tmp/deadline_app"):
        os.mkdir("/tmp/deadline_app")

    with open("/tmp/deadline_app/my.ics", "w") as my_file:
        my_file.writelines(c.serialize_iter())
    return send_file("/tmp/deadline_app/my.ics", attachment_filename="my.ics")
