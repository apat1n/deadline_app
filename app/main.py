import os
from datetime import datetime

from flask import Flask, redirect, render_template, request, url_for

from app.db import (DeadlineEvent, add_event, db, delete_event,
                    get_event_by_id, get_events)
from app.ics_file import generate_ics_file

basedir = ""
app = Flask(__name__)
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///" + os.path.join(
    basedir, "database.db"
)
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.jinja_options['extensions'] = ['jinja2_humanize_extension.HumanizeExtension']


@app.route("/")
def index():
    events = get_events()
    sorted(get_events(), key=lambda e: e.deadline)
    return render_template("index.html", event_list=events, datetime_now=datetime.now())


@app.route("/add_deadline", methods=["GET", "POST"])
def add_deadline():
    if request.method == "GET":
        return render_template("add_deadline.html")

    add_event(
        DeadlineEvent(
            name=request.form["deadline-name"],
            deadline=datetime.fromisoformat(request.form["deadline-time"]),
        )
    )
    return redirect(url_for("index"))


@app.route("/delete_deadline/<int:event_id>", methods=["POST"])
def delete_deadline(event_id: int):
    delete_event(event_id)
    return redirect(url_for("index"))


@app.route("/download_ics_file/<int:event_id>", methods=["GET"])
def download_ics_file(event_id: int):
    return generate_ics_file(get_event_by_id(event_id))


def create_test_app():
    app = Flask(__name__)
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///" + os.path.join(
        basedir, "test_database.db"
    )
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    # Dynamically bind SQLAlchemy to application
    db.init_app(app)
    app.app_context().push()  # this does the binding
    return app


if __name__ == "__main__":
    db.init_app(app)
    with app.app_context():
        db.create_all()
    app.run("localhost", 5050, debug=True)
