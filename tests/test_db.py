import datetime
import unittest

import app.main
from app.db import (DeadlineEvent, add_event, db, delete_event,
                    get_event_by_name)

NUM_ITERATIONS = 10


class MyTest(unittest.TestCase):
    TESTING = True

    def create_app(self):
        # pass in test configuration
        return app.main.create_test_app()

    def setUp(self):
        self.create_app()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_adding(self):
        for i in range(NUM_ITERATIONS):
            event = DeadlineEvent(name=f"test_{i}", deadline=datetime.datetime.now())
            add_event(event)

        events = DeadlineEvent.query.filter(DeadlineEvent.name.like("test_%")).all()
        assert len(events) == 10

    def test_delete(self):
        event = DeadlineEvent(name="test_delete", deadline=datetime.datetime.now())
        add_event(event)

        event = get_event_by_name("test_delete")
        delete_event(event.id)
